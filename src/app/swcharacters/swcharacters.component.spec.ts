import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwcharactersComponent } from './swcharacters.component';

describe('SwcharactersComponent', () => {
  let component: SwcharactersComponent;
  let fixture: ComponentFixture<SwcharactersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwcharactersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwcharactersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
