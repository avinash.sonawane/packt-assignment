import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-swcharacters',
  templateUrl: './swcharacters.component.html',
  styleUrls: ['./swcharacters.component.css']
})
export class SwcharactersComponent implements OnInit {
  characters: any = [];
  public searchText: string;
  public height: number = 0;
  constructor(private api: ApiService) { 
  }

  ngOnInit() {
    this.getCharacters();
  }

  public getCharacters(){
    this.api.getCharacters().subscribe((data: any) => {
        this.characters = data.results;
        this.height = this.characters.reduce((total, next) => total + parseInt(next.height), 0) / this.characters.length;
    });
  }
}
