import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SwcharactersComponent } from './swcharacters/swcharacters.component';

const routes: Routes = [
  {
    path: 'swcharacters',
    component: SwcharactersComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
