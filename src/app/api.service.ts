import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
	
	httpOptions = {
	  headers: new HttpHeaders({'Content-Type': 'application/json'})
	};
	uri = 'https://swapi.co/api';

	constructor(private http: HttpClient) { }

	getCharacters() {
    	return this.http.get(`${this.uri}/people`);
  }
}
