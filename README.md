# Packt Assignment

Packt Publishing Assignment angular and Node.
=======
# PacktAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.5.

# Task 1 - Questions and Answers
	Please refer question.txt  file inside app code folder for all answers.

# Task 2 - Fizz Problem
	Please refer fizz.js inside the app code folder for demo. Simply run it using command node fizz.js

# Task 3 - Angular App

## Development server
1.Download the repo
2.run command npm i
3.run command ng serve -o on your terminal. 

Note - Application is running on http://localhost:4200
Note - Task 3 section no 3rd and 5th is pending due to queries.


